// po wpisaniu implements ten interface czyli info klikasz w error i pierwsze z gory
// pojawia sie wtedy ten override
// mozna po przecinku zrobic oprocz info nastepny interface i bedzie tego duzo
// oprocz tego mozna dodac extends jak wczesniej ale tylko jedno
public class Machine implements Info {

	private int id = 7;
	
	public void start(){
		
		System.out.println("Machine started");
		
	}

	@Override
	public void showInfo() {

		System.out.println("Machine ID is: " + id);
		
	}
}
