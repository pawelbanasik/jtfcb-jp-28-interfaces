
public class App {

	public static void main(String[] args) {

		Machine mach1 = new Machine();
		mach1.start();

		Person person1 = new Person("Bob");
		person1.greet();

		// mozna teraz uzywac tego intreface'u
		// tylko nie mozna pisac interface'u w czesci tam gdzie jest new
		Info info1 = new Machine();
		info1.showInfo();

		Info info2 = person1;
		info2.showInfo();

		outputInfo(mach1);
		outputInfo(person1);

	}

	// zrobil static akurat ta dlatego ze nie ma app object
	// to jest pierwsze zastosowanie interafac�w
	// powiedzial ze inne na necie zobaczyc jakie sa zastosowania
	// powiedzial ze niektorzy projektuja cale programy uzywajac interface
	// niektozry najpierw zaczynaja od interface�w i tak sobie projektuja

	private static void outputInfo(Info info) {
		info.showInfo();
	}

}
